/*******************************************************************************
 * Copyright 2013 Chevil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.alphasigma.scratchimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This view start with full gray color bitmap and onTouch to make it
 * transparent
 * 
 * @author winsontan520
 */
public class WTimerView extends SurfaceView implements SurfaceHolder.Callback {

	private static final String TAG = "Scratch Celebrities";

	private Context mContext;
	private PlayGame mActivity = null;
	private int mCount = 60;
	private int mActual;

	public WTimerView(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
		reset();
		SurfaceHolder holder = getHolder();
		setZOrderOnTop(true);
		holder.addCallback(this);
		holder.setFormat(PixelFormat.TRANSPARENT);
		// Log.v( TAG, "Timer View created" );
	}

	public void setCount(int count) {
		if (count <= 0) {
			Log.e(TAG, "error : wrong count : " + count);
			return;
		}
		mCount = count;
	}

	public void setActivity(PlayGame activity) {
		mActivity = activity;
	}

	public void reset() {
		mActual = mCount;
	}

	public int tick() {

		Canvas c = null;
		int ret;

		if (mActual > 0) {
			mActual--;
			ret = mActual;
		} else {
			Log.e(TAG, "Timer : already expired");
			ret = -1;
		}
		try {
			c = getHolder().lockCanvas(null);
			synchronized (getHolder()) {
				if (c != null) {
					onDraw(c);
				}
			}
		} finally {
			if (c != null) {
				getHolder().unlockCanvasAndPost(c);
			}
		}
		return ret;
	}

	@Override
	public void onDraw(Canvas canvas) {
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		RectF rect = new RectF(0, 0, height, height);
		Paint wpaint = new Paint();
		if (mActual > mCount / 3) {
			wpaint.setColor(Color.YELLOW);
		} else if (mActual > mCount / 5) {
			wpaint.setColor(Color.argb(255, 253, 163, 5));
		} else {
			wpaint.setColor(Color.RED);
		}
		wpaint.setStrokeWidth(2);
		wpaint.setTextSize(30);

		if (mActivity != null) {
			Bitmap bckgnd = BitmapFactory.decodeResource(
					mActivity.getResources(), R.drawable.timerbg);
			canvas.drawBitmap(bckgnd,
					new Rect(0, 0, bckgnd.getWidth(), bckgnd.getHeight()),
					new RectF(0, 0, width, height), new Paint());
		}
		canvas.drawArc(rect, -90.0f, -((float) mActual / (float) mCount) * 360,
				true, wpaint);
		canvas.drawText("" + mActual, height + 10, height - (height - 30) / 2,
				wpaint);
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// do nothing
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}

}
