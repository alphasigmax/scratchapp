package com.alphasigma.scratchimage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alphasigma.scratchimage.inappbilling.util.IabHelper;

public class PlayGame extends Activity {

	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String TAG = "Scratch Celebrities";
	public static final String mJSONFile = "enigmas";

	private DESEncrypt mEncrypt = new DESEncrypt();
	private Activity mActivity;
	private AudioManager audio;
	WScratchView scratchView;
	WTimerView timerView = null;
	ProgressBar progressBar;
	TextView meter, cointext, leveltext;
	TextView[] blocks = new TextView[21]; // 9 answers + 12 letters

	ImageView delete, hint, winner, whenwin, awesome, better, good, baselevel;

	// Hint Section Imageviews
	ImageView cancel, getmorecoins, uncoverpic, fiftycoin, revalletter,
			fiftycoin1, removeletter, seventyfive, cancel1, oneooo,
			ooonetriplenine, fourfiveoocoin, ootriplenine, twotripleocoins,
			ofourninenine, sevenfivezerocons, oooneninenine,
			threefivezerocoins, ooninenine, answerb, frame, blankbar, header,
			back, level, coinimage;
	int mProgress = 100;
	int mMinProgress = 25;
	int mCoins = 100;

	private String gettingcoin;
	private String jsenigmas;
	private int mNbEnigmas = -1;
	private static long mAlbum = -1;
	private static long mLevel = -1;
	private String mImage = "";
	private String mEndImage = "";
	private String mAnswer = "";
	private String mResponse = "";
	private SharedPreferences mPrefs = null;
	private Bitmap mBitmap;
	private Bitmap mEndBitmap;
	private int mRevealSize = 10;
	private Handler mHandler = null;

	// in-app billing
	private static String appKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlsat8RmPDfO7ygcRtY0wz6SNjg9WXmoZ6J1ClDD0drKzDfGTbxWpxaGdwHpy42vbih0Pbnt9SFld08ZzTO0ESWvKcs3QfC7Sgc+MeAAUioH6HmGhqwZfb5DXXDn1IgI98UsPwWBnnm1Fu9aY4ZPGRrH7Iog0GgYo31lqqQKhyIUAFr0KtsxBxaEkWnLfWe21Jfk6eSKG5g4qLQ5EcUztnDuIAAMcq5ka5qyGgqMAvVgys19Q5LrK0KtC0xnVcWwo5WtnpxtWS9tfA/GMc03UQcYMfQT+ihjzEpCJogKZYIHDS4oBqYaQifDmC30DWnU280UxvckvCvug0QAWt3N/vwIDAQAB";
	public static IabHelper mHelper = null;

	private Runnable mUpdateTimer = new Runnable() {
		@Override
		public void run() {
			if (timerView != null) {
				int remaining = timerView.tick();
				if (remaining > 0 && mHandler != null) {
					mHandler.postDelayed(this, 1000);
				} else {
					// timer elapsed
					if (mCoins > 0) {
						mCoins--;
						cointext.setText("" + mCoins);
						Log.e("mCoins", mCoins + "");
						mPrefs.edit()
								.putString("CoinValue",
										mEncrypt.encrypt("" + mCoins)).commit();
					}

					final CharSequence[] items = { "Retry this level",
							"Go back to Main Menu" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mActivity);
					builder.setTitle("Time's up! Lost one coin! \n Now, what do you want to do?");
					builder.setCancelable(false);
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int item) {

									if (items[item] == "Retry this level") {
										settingview();
									}

									if (items[item] == "Go back to Main Menu") {
										startActivity(new Intent(mActivity,
												LevelActivity.class));
										mActivity.finish();
									}
								}
							});
					AlertDialog alert = builder.create();
					alert.show();
				}
			}
		}
	};

	private Runnable mUpdateCoins = new Runnable() {
		@Override
		public void run() {
			if (cointext != null) {
				/*
				 * Default amount is set to 500 Date : 5th Feb,2013
				 */
				String gettingcoin = mEncrypt.decrypt(mPrefs.getString(
						"CoinValue", mEncrypt.encrypt("500")));
				cointext.setText(gettingcoin);
				mCoins = Integer.parseInt(gettingcoin);
			}
			if (mHandler != null) {
				mHandler.postDelayed(this, 1000);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mPrefs = getSharedPreferences(LevelActivity.PREFS_NAME, 0);

		// getting the desired level
		Bundle receiveParams = getIntent().getExtras();
		try {
			mAlbum = receiveParams.getLong("album");
		} catch (Exception e) {
			Log.v(TAG, "couldn't get album");
		}

		if (mAlbum == -1) {
			Log.e(TAG, "couldn't initialize activity");
			finish();
		} else {
			Log.v(TAG, "starting play game with album : " + mAlbum);
		}

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (mHelper == null) {
			mHelper = new IabHelper(this, appKey);
		}

		if (mHandler == null) {
			mHandler = new Handler();
		}

		// show window and start timer
		settingview();

		Log.v(TAG, "play game created");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.v(TAG, "play game resumed");
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		Log.v(TAG, "destroying activity");
		super.onDestroy();
		if (mHandler != null) {
			Log.v(TAG, "removing callbacks");
			mHandler.removeCallbacks(mUpdateTimer);
			mHandler.removeCallbacks(mUpdateCoins);
			mHandler = null;
		}
		if (mHelper != null) {
			try {
				Log.v(TAG, "disposing purchase");
				mHelper.dispose();
				mHelper = null;
			} catch (Exception e) {
				Log.e(TAG, "could not dispose billing", e);
			}
		}
		Log.v(TAG, "activity destroyed");
	}

	public void initImage() {
		boolean showImage;
		if (mEncrypt.decrypt(
				mPrefs.getString("ShowImage" + mAlbum, mEncrypt.encrypt("n")))
				.equals("y")) {
			showImage = true;
			scratchView.setScratchable(false);
		} else {
			showImage = false;
			scratchView.setScratchable(true);
		}
		Log.v(TAG, "showing image : " + showImage);
		scratchView.setActivity(this);
		scratchView.setRevealSize(mRevealSize);
		scratchView.setOverlayColor(Color.LTGRAY);
		scratchView.setBitmap(mBitmap, showImage);
	}

	public void initLetters() {
		int nbRevealed = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
				"NbRevealed" + mAlbum, mEncrypt.encrypt("0"))));
		boolean hideLetters;
		if (mEncrypt
				.decrypt(
						mPrefs.getString("HideLetters" + mAlbum,
								mEncrypt.encrypt("n"))).equals("y")) {
			hideLetters = true;
		} else {
			hideLetters = false;
		}

		// Log.v( TAG, "init letters : hide : " + hideLetters );

		for (int ib = 0; ib < nbRevealed; ib++) {
			blocks[ib].setText("" + mAnswer.charAt(ib));
			blocks[ib].setBackgroundResource(R.drawable.white_filed1);
		}

		for (int ib = nbRevealed; ib < 9; ib++) {
			blocks[ib].setText("");
			blocks[ib].setBackgroundColor(Color.TRANSPARENT);
			if (ib >= mAnswer.length()) {
				blocks[ib].setVisibility(View.INVISIBLE);
			}
		}

		// setting random letters first
		Random r = new Random();
		String rletters = mAnswer;
		if (mAnswer.length() > 12) {
			Log.e(TAG, "error : answer is too long : " + mAnswer);
		}
		for (int il = mAnswer.length(); il < 12; il++) {
			if (!hideLetters) {
				rletters += (char) (r.nextInt(26) + 'A');
			} else {
				rletters += '-';
			}
		}

		// Log.v( TAG, "random letters : " + rletters );
		char[] cletters = rletters.toCharArray();
		for (int ib = 0; ib < nbRevealed; ib++) {
			cletters[ib] = '-';
		}

		for (int ib = 9; ib < 21; ib++) {
			boolean filled = false;
			while (filled == false) {
				int index = r.nextInt(12);
				if (cletters[index] != ' ') {
					if (cletters[index] != '-') {
						blocks[ib].setText("" + cletters[index]);
						blocks[ib].setVisibility(View.VISIBLE);
					} else {
						blocks[ib].setText("");
						blocks[ib].setVisibility(View.INVISIBLE);
					}
					cletters[index] = ' ';
					filled = true;
				}
			}
		}
	}

	public void settingview() {
		// position widgets proportionally
		Display display = getWindowManager().getDefaultDisplay();
		final int swidth = display.getWidth();
		final int sheight = display.getHeight();
		final int scratchWidth = (int) (swidth * 0.6); // screen dimensions
		final int scratchHeight = (int) (sheight * 0.4);

		try {
			jsenigmas = loadJSONFromAsset(mJSONFile + mAlbum + ".json");
			// Log.v( TAG, "got the enigmas : " + jsenigmas );
			JSONArray json = new JSONArray(jsenigmas);
			mNbEnigmas = json.length();

			mLevel = Long.parseLong(mEncrypt.decrypt(mPrefs.getString("Level"
					+ mAlbum, mEncrypt.encrypt("-1"))));
			if (mLevel >= mNbEnigmas || mLevel == -1) {
				Log.v(TAG, "requested level is wrong");
				finish();
			}
			JSONObject jenigma = json.getJSONObject((int) mLevel);
			mImage = jenigma.getString("image");
			if (jenigma.has("answerimage")) {
				mEndImage = jenigma.getString("answerimage");
			} else {
				mEndImage = mImage;
			}
			mAnswer = jenigma.getString("answer").toUpperCase();
			Log.v(TAG, "starting activity with level : " + mLevel);
		} catch (Exception e) {
			Log.e(TAG, "couldn't read the JSON file", e);
			finish();
		}

		// load bitmap of this celebrity
		try {
			InputStream ibitmap = getAssets().open(mImage);
			Bitmap tBitmap = BitmapFactory.decodeStream(ibitmap);
			Log.v(TAG,
					"got an image of " + tBitmap.getWidth() + "x"
							+ tBitmap.getHeight());
			Log.v(TAG, "resizing to " + scratchWidth + "x" + scratchHeight);
			float scaleWidth = ((float) scratchWidth) / tBitmap.getWidth();
			float scaleHeight = ((float) scratchHeight) / tBitmap.getHeight();
			Matrix matrix = new Matrix();
			matrix.postScale(scaleWidth, scaleHeight);
			mBitmap = Bitmap.createBitmap(tBitmap, 0, 0, tBitmap.getWidth(),
					tBitmap.getHeight(), matrix, true);
			ibitmap.close();

			ibitmap = getAssets().open(mEndImage);
			tBitmap = BitmapFactory.decodeStream(ibitmap);
			Log.v(TAG, "got an end image of " + tBitmap.getWidth() + "x"
					+ tBitmap.getHeight());
			Log.v(TAG, "resizing to " + swidth + "x" + sheight);
			scaleWidth = ((float) swidth) / tBitmap.getWidth();
			scaleHeight = ((float) sheight) / tBitmap.getHeight();
			matrix = new Matrix();
			matrix.postScale(scaleWidth, scaleHeight);
			mEndBitmap = Bitmap.createBitmap(tBitmap, 0, 0, tBitmap.getWidth(),
					tBitmap.getHeight(), matrix, true);
			ibitmap.close();

		} catch (Exception e) {
			Log.e(TAG, "couldn't create bitmap", e);
			finish();
		}

		setContentView(R.layout.activity_play_game);

		hint = (ImageView) findViewById(R.id.hint);
		answerb = (ImageView) findViewById(R.id.answerb);
		delete = (ImageView) findViewById(R.id.delete);
		level = (ImageView) findViewById(R.id.level);
		leveltext = (TextView) findViewById(R.id.leveltext);
		scratchView = (WScratchView) findViewById(R.id.scratch_view);
		timerView = (WTimerView) findViewById(R.id.timer_view);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		meter = (TextView) findViewById(R.id.meter);
		frame = (ImageView) findViewById(R.id.frame);
		blankbar = (ImageView) findViewById(R.id.blankbar);
		header = (ImageView) findViewById(R.id.header);
		back = (ImageView) findViewById(R.id.back);
		coinimage = (ImageView) findViewById(R.id.coinimage);
		cointext = (TextView) findViewById(R.id.cointext);

		winner = (ImageView) findViewById(R.id.winnerview);
		whenwin = (ImageView) findViewById(R.id.whenwin);
		awesome = (ImageView) findViewById(R.id.awesome);
		better = (ImageView) findViewById(R.id.better);
		baselevel = (ImageView) findViewById(R.id.baselevel);
		good = (ImageView) findViewById(R.id.good);

		progressBar.setProgress(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgress(100);
		progressBar.setMax(100);
		audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		initImage();
		setScratchProgress(100);

		float textSize = getResources().getDimension(R.dimen.text_size);
		textSize = 20;
		Log.v(TAG, "text size is : " + textSize);
		leveltext.setText("" + (mLevel + 1));

		Typeface type = Typeface.createFromAsset(getAssets(), "font.ttf");
		meter.setTypeface(type);
		meter.setText("scratch meter");
		meter.setTextSize(15);

		cointext.setTypeface(type);
		cointext.setTextSize(textSize);
		gettingcoin = mEncrypt.decrypt(mPrefs.getString("CoinValue",
				mEncrypt.encrypt("0")));
		cointext.setText(gettingcoin);
		mCoins = Integer.parseInt(gettingcoin);

		// answers
		blocks[0] = (TextView) findViewById(R.id.block1);
		blocks[1] = (TextView) findViewById(R.id.block2);
		blocks[2] = (TextView) findViewById(R.id.block3);
		blocks[3] = (TextView) findViewById(R.id.block4);
		blocks[4] = (TextView) findViewById(R.id.block5);
		blocks[5] = (TextView) findViewById(R.id.block6);
		blocks[6] = (TextView) findViewById(R.id.block7);
		blocks[7] = (TextView) findViewById(R.id.block8);
		blocks[8] = (TextView) findViewById(R.id.block9);

		// letters
		blocks[9] = (TextView) findViewById(R.id.block10);
		blocks[10] = (TextView) findViewById(R.id.block11);
		blocks[11] = (TextView) findViewById(R.id.block12);
		blocks[12] = (TextView) findViewById(R.id.block13);
		blocks[13] = (TextView) findViewById(R.id.block14);
		blocks[14] = (TextView) findViewById(R.id.block15);
		blocks[15] = (TextView) findViewById(R.id.block16);
		blocks[16] = (TextView) findViewById(R.id.block17);
		blocks[17] = (TextView) findViewById(R.id.block18);
		blocks[18] = (TextView) findViewById(R.id.block19);
		blocks[19] = (TextView) findViewById(R.id.block20);
		blocks[20] = (TextView) findViewById(R.id.block21);

		for (int ib = 0; ib < 21; ib++) {
			blocks[ib].setTypeface(type);
			blocks[ib].setTextSize(textSize);
		}

		initLetters();

		timerView.setActivity(this);
		timerView.setCount(60);
		timerView.reset();

		if (mHandler != null) {
			mHandler.postDelayed(mUpdateTimer, 1000);
			mHandler.postDelayed(mUpdateCoins, 1000);
		}

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.PlayLayout);
		RelativeLayout.LayoutParams lp;

		lp = new RelativeLayout.LayoutParams(swidth, (int) (sheight * 0.10));
		lp.leftMargin = 0;
		lp.topMargin = 0;
		rl.updateViewLayout(header, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.25),
				(int) (sheight * 0.08));
		lp.leftMargin = (int) (swidth * 0.05);
		lp.topMargin = (int) (sheight * 0.01);
		rl.updateViewLayout(back, lp);

		lp = new RelativeLayout.LayoutParams((int) (sheight * 0.08),
				(int) (sheight * 0.08));
		lp.leftMargin = (int) (swidth * 0.45);
		lp.topMargin = (int) (sheight * 0.01);
		rl.updateViewLayout(level, lp);

		lp = new RelativeLayout.LayoutParams((int) (sheight * 0.08),
				(int) (sheight * 0.08));
		lp.leftMargin = (int) (swidth * 0.45);
		lp.topMargin = (int) (sheight * 0.01);
		rl.updateViewLayout(leveltext, lp);

		lp = new RelativeLayout.LayoutParams((int) (sheight * 0.08),
				(int) (sheight * 0.08));
		lp.leftMargin = (int) (swidth * 0.65);
		lp.topMargin = (int) (sheight * 0.01);
		rl.updateViewLayout(coinimage, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.2),
				(int) (sheight * 0.08));
		lp.leftMargin = (int) (swidth * 0.8);
		lp.topMargin = (int) (sheight * 0.03);
		rl.updateViewLayout(cointext, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.6),
				(int) (sheight * 0.05));
		lp.leftMargin = (int) (swidth * 0.2 / 2);
		lp.topMargin = (int) (sheight * 0.12);
		rl.updateViewLayout(progressBar, lp);
		rl.updateViewLayout(meter, lp);
		rl.updateViewLayout(blankbar, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.2),
				(int) (sheight * 0.05));
		lp.leftMargin = (int) (swidth * 0.75);
		lp.topMargin = (int) (sheight * 0.12);
		rl.updateViewLayout(timerView, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.60),
				(int) (sheight * 0.40));
		lp.leftMargin = (int) (swidth * 0.4 / 2);
		lp.topMargin = (int) (sheight * 0.20);
		rl.updateViewLayout(scratchView, lp);

		lp = new RelativeLayout.LayoutParams((int) (swidth * 0.68),
				(int) (sheight * 0.45));
		lp.leftMargin = (int) (swidth * 0.35 / 2);
		lp.topMargin = (int) (sheight * 0.185);
		rl.updateViewLayout(frame, lp);

		int nbInputs = mAnswer.length();
		for (int ib = 0; ib < 9; ib++) {
			lp = new RelativeLayout.LayoutParams((int) ((swidth - 10) / 10),
					(int) ((swidth - 10) / 10));
			lp.leftMargin = (swidth - nbInputs * (int) (swidth - 10) / 10) / 2
					+ (int) ((swidth - 10) / 10) * ib;
			lp.topMargin = (int) (sheight * 0.65);
			rl.updateViewLayout(blocks[ib], lp);
		}

		lp = new RelativeLayout.LayoutParams((int) ((swidth - 10) / 10),
				(int) ((swidth - 10) / 10));
		lp.leftMargin = 5 + (int) ((swidth - 10) / 10) * 9;
		lp.topMargin = (int) (sheight * 0.65);
		rl.updateViewLayout(delete, lp);

		lp = new RelativeLayout.LayoutParams(swidth,
				(int) ((swidth - 10) / 10) + 10);
		lp.leftMargin = 0;
		lp.topMargin = (int) (sheight * 0.65) - 5;
		rl.updateViewLayout(answerb, lp);

		for (int ib = 9; ib < 15; ib++) {
			lp = new RelativeLayout.LayoutParams((int) ((swidth - 10) / 7),
					(int) ((swidth - 10) / 7));
			lp.leftMargin = 5 + (int) ((swidth - 10) / 7) * (ib - 9);
			lp.topMargin = (int) (sheight * 0.72);
			rl.updateViewLayout(blocks[ib], lp);
		}

		for (int ib = 15; ib < 21; ib++) {
			lp = new RelativeLayout.LayoutParams((int) ((swidth - 10) / 7),
					(int) ((swidth - 10) / 7));
			lp.leftMargin = 5 + (int) ((swidth - 10) / 7) * (ib - 15);
			lp.topMargin = (int) (sheight * 0.82);
			rl.updateViewLayout(blocks[ib], lp);
		}

		lp = new RelativeLayout.LayoutParams((int) ((swidth - 10) / 7),
				2 * (int) ((swidth - 10) / 7));
		lp.leftMargin = 5 + (int) ((swidth - 10) / 7) * 6;
		lp.topMargin = (int) (sheight * 0.72);
		rl.updateViewLayout(hint, lp);

		lp = new RelativeLayout.LayoutParams(swidth, (int) (sheight * 0.2));
		lp.leftMargin = 0;
		lp.topMargin = (int) (sheight * 0.70);
		rl.updateViewLayout(baselevel, lp);
		rl.updateViewLayout(good, lp);
		rl.updateViewLayout(awesome, lp);
		rl.updateViewLayout(better, lp);

		/**
		 * topMargin : Changed topMargin height due to problem in showing
		 * appreciation message.
		 */

		lp = new RelativeLayout.LayoutParams(2 * swidth / 3,
				(int) (sheight * 0.12));
		lp.leftMargin = swidth / 6;
		lp.topMargin = (int) (sheight * 0.77);
		rl.updateViewLayout(winner, lp);

		lp = new RelativeLayout.LayoutParams(swidth, sheight);
		lp.leftMargin = 0;
		lp.topMargin = 0;
		rl.updateViewLayout(whenwin, lp);
	}

	public void blocka(View v) {
		TextView t = (TextView) v;
		OurSoundPlayer.playSound(this, R.raw.clang);
		if (!t.getText().toString().equals("")) {
			String s1 = t.getText().toString();
			t.setText("");
			t.setBackgroundColor(Color.TRANSPARENT);
			settingbackvalues(s1);
		}
	}

	public void blockl(View v) {
		TextView t = (TextView) v;
		OurSoundPlayer.playSound(this, R.raw.clang);
		String s = t.getText().toString();
		t.setVisibility(View.INVISIBLE);
		t.setText("");
		settingName(s);
	}

	public void settingName(String s) {

		int posindex = -1;

		for (int ib = 0; ib < 9; ib++) {
			if (blocks[ib].getText().toString().equals("")) {
				blocks[ib].setBackgroundResource(R.drawable.white_filed1);
				blocks[ib].setText(s);
				posindex = ib;
				break;
			}
		}

		if (posindex + 1 >= mAnswer.length()) {
			mResponse = "";
			for (int ib = 0; ib <= posindex; ib++) {
				mResponse += blocks[ib].getText();
			}
			checkforanswer();
		}

	}

	public void checkforanswer() {
		Log.v(TAG, "checking for answer : " + mResponse);
		if (mAnswer.equalsIgnoreCase(mResponse)) {
			if (mHandler != null) {
				mHandler.removeCallbacks(mUpdateTimer);
			}
			scratchView.setScratchable(false);
			hint.setVisibility(View.INVISIBLE);
			delete.setVisibility(View.INVISIBLE);
			answerb.setVisibility(View.INVISIBLE);
			for (int ib = 9; ib < 21; ib++) {
				blocks[ib].setVisibility(View.INVISIBLE);
			}
			baselevel.setVisibility(View.VISIBLE);
			mCoins = mCoins
					+ ((mProgress - mMinProgress) * 9 / (100 - mMinProgress))
					+ 1;
			Log.v(TAG,
					"adding "
							+ (((mProgress - mMinProgress) * 9 / (100 - mMinProgress)) + 1)
							+ " coins. mProgress : " + mProgress);
			mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + mCoins))
					.commit();
			if (mProgress >= 65) {
				awesome.setVisibility(View.VISIBLE);
			} else if (mProgress >= 30) {
				better.setVisibility(View.VISIBLE);
			} else {
				good.setVisibility(View.VISIBLE);
			}
			inclevel();
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					delete.setClickable(false);
					for (int ib = 0; ib < 9; ib++) {
						blocks[ib].setClickable(false);
					}
					good.setVisibility(View.INVISIBLE);
					better.setVisibility(View.INVISIBLE);
					awesome.setVisibility(View.INVISIBLE);
					OurSoundPlayer.playSound(PlayGame.this, R.raw.coin_rake);
					cointext.setText("" + mCoins);
					mPrefs.edit()
							.putString("CoinValue",
									mEncrypt.encrypt("" + mCoins)).commit();
					winner.setVisibility(View.VISIBLE);
					whenwin.setImageBitmap(mEndBitmap);
					whenwin.setVisibility(View.VISIBLE);
					scratchView.setVisibility(View.INVISIBLE);
					timerView.setVisibility(View.INVISIBLE);
				}
			}, 3000);
		} else {
			delete(delete);
		}
	}

	public void setScratchProgress(int progress) {
		mProgress = progress;
		progressBar.setProgress(mProgress);
		if (mProgress < mMinProgress) {
			scratchView.setScratchable(false);
			OurSoundPlayer.playSound(this, R.raw.clang);
		}
	}

	public void delete(View v) {

		OurSoundPlayer.playSound(this, R.raw.clang);

		for (int ib = 0; ib < 9; ib++) {
			if (!blocks[ib].getText().toString().equals("")) {
				String s1 = blocks[ib].getText().toString();
				blocks[ib].setBackgroundColor(Color.TRANSPARENT);
				blocks[ib].setText("");
				settingbackvalues(s1);
			}
		}
		initLetters();
	}

	public void settingbackvalues(String s1) {

		for (int ib = 9; ib < 21; ib++) {
			if (blocks[ib].getVisibility() == View.INVISIBLE) {
				blocks[ib].setVisibility(View.VISIBLE);
				blocks[ib].setBackgroundResource(R.drawable.big_white_field1);
				blocks[ib].setText(s1);
				break;
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			return true;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}

	public void back(View v) {
		OurSoundPlayer.playSound(this, R.raw.clang);
		startActivity(new Intent(PlayGame.this, LevelActivity.class));
		Log.v(TAG, "finishing activity");
		this.finish();
	}

	public void showHelpers(View v) {
		ShowMenus.showHelpers(this, mHelper);
	}

	public void showGetCoins(View v) {
		ShowMenus.showGetCoins(this, mHelper);
	}

	public void nextlevel(View v) {
		Log.v(TAG, "next : enigma : " + mLevel + "/" + mNbEnigmas);
		if (mLevel == mNbEnigmas) {
			Intent i = new Intent(PlayGame.this, LevelActivity.class);
			startActivity(i);
			finish();
		} else {
			settingview();
		}
	}

	public void inclevel() {
		mLevel = mLevel + 1;
		mPrefs.edit()
				.putString("Level" + mAlbum, mEncrypt.encrypt("" + mLevel))
				.commit();
		Log.v(TAG, "enigma : " + mLevel + "/" + mNbEnigmas);
		if (mLevel == mNbEnigmas) {
			int nbLevels = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
					"NbLevels", mEncrypt.encrypt("0"))));

			Log.v(TAG, "album : " + (mAlbum + 1) + "/" + nbLevels);
			if (mAlbum + 1 == nbLevels) {
				SharedPreferences.Editor editor = mPrefs.edit();
				// reset the game
				for (int il = 0; il < nbLevels; il++) {
					editor.putString("Level" + il, mEncrypt.encrypt("0"));
					editor.putString("NbRevealed" + il, mEncrypt.encrypt("0"));
					editor.putString("HideLetters" + il, mEncrypt.encrypt("n"));
					editor.putString("ShowImage" + il, mEncrypt.encrypt("n"));
					if (il == 0) {
						editor.putString("Lock" + il, mEncrypt.encrypt("" + il));
					} else {
						editor.putString("Lock" + il,
								mEncrypt.encrypt("-" + il));
					}
				}
				editor.commit();
			} else {
				mPrefs.edit()
						.putString("Lock" + (mAlbum + 1),
								mEncrypt.encrypt("" + (mAlbum + 1))).commit();
				mPrefs.edit()
						.putString("Lock" + mAlbum,
								mEncrypt.encrypt("-" + mAlbum)).commit();
				mPrefs.edit()
						.putString("NbRevealed" + mAlbum, mEncrypt.encrypt("0"))
						.commit();
				mPrefs.edit()
						.putString("HideLetters" + mAlbum,
								mEncrypt.encrypt("n")).commit();
				mPrefs.edit()
						.putString("ShowImage" + mAlbum, mEncrypt.encrypt("n"))
						.commit();
			}
		} else {
			mPrefs.edit()
					.putString("NbRevealed" + mAlbum, mEncrypt.encrypt("0"))
					.commit();
			mPrefs.edit()
					.putString("HideLetters" + mAlbum, mEncrypt.encrypt("n"))
					.commit();
			mPrefs.edit()
					.putString("ShowImage" + mAlbum, mEncrypt.encrypt("n"))
					.commit();
		}
	}

	public String loadJSONFromAsset(String filename) {
		String json = null;
		try {
			InputStream is = getAssets().open(filename);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			json = new String(buffer, "UTF-8");
		} catch (IOException ex) {
			Log.e(TAG, "couldn't read json from assets");
			return null;
		}
		return json;
	}

	public void revealLetter() {
		if (mCoins < 50) {
			Toast.makeText(PlayGame.this,
					"You don't have enough coins to do that, get more coins!",
					Toast.LENGTH_LONG).show();
			ShowMenus.showGetCoins(this, mHelper);
			return;
		}
		int nbRevealed = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
				"NbRevealed" + mAlbum, mEncrypt.encrypt("0"))));
		if (nbRevealed >= mAnswer.length()) {
			Toast.makeText(PlayGame.this,
					"All the letters have been revealed!", Toast.LENGTH_LONG)
					.show();
			return;
		} else {
			for (int ib = nbRevealed; ib < 9; ib++) {
				if (!blocks[ib].getText().equals("")) {
					if (blocks[ib].getText().charAt(0) == mAnswer.charAt(ib)) {
						nbRevealed++;
					}
				}
				// Log.v( TAG,
				// ""+blocks[ib].getText().charAt(0)+" == "+mAnswer.charAt(ib)
				// );
			}
			nbRevealed += 1;
			mCoins -= 50;
			mPrefs.edit()
					.putString("NbRevealed" + mAlbum,
							mEncrypt.encrypt("" + nbRevealed)).commit();
			mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + mCoins))
					.commit();
			OurSoundPlayer.playSound(this, R.raw.clang);
			initLetters();
			if (nbRevealed + 1 >= mAnswer.length()) {
				mResponse = "";
				for (int ib = 0; ib <= nbRevealed; ib++) {
					mResponse += blocks[ib].getText();
				}
				checkforanswer();
			}
		}
	}

	public void hideLetters() {
		if (mCoins < 75) {
			Toast.makeText(PlayGame.this,
					"You don't have enough coins to do that, get more coins!",
					Toast.LENGTH_LONG).show();
			ShowMenus.showGetCoins(this, mHelper);
			return;
		} else {
			mCoins -= 75;
			mPrefs.edit()
					.putString("HideLetters" + mAlbum, mEncrypt.encrypt("y"))
					.commit();
			mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + mCoins))
					.commit();
			initLetters();
		}
	}

	public void showImage() {
		if (mCoins < 50) {
			Toast.makeText(PlayGame.this,
					"You don't have enough coins to do that, get more coins!",
					Toast.LENGTH_LONG).show();
			ShowMenus.showGetCoins(this, mHelper);
			return;
		} else {
			mCoins -= 50;
			mPrefs.edit()
					.putString("ShowImage" + mAlbum, mEncrypt.encrypt("y"))
					.commit();
			mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + mCoins))
					.commit();
			initImage();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper == null
				|| !mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

}
