/*******************************************************************************
 * Copyright 2013 Chevil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.alphasigma.scratchimage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alphasigma.scratchimage.inappbilling.util.IabHelper;
import com.alphasigma.scratchimage.inappbilling.util.IabResult;
import com.alphasigma.scratchimage.inappbilling.util.Inventory;
import com.alphasigma.scratchimage.inappbilling.util.Purchase;

/**
 * Utility class to show various menus
 * 
 * @author chevil
 */
public class ShowMenus {

	private static final String TAG = "Scratch Celebrities";

	private Context mContext;
	private static Activity mCallingActivity;
	private static IabHelper mHelper;
	private static boolean mIabSetup = false;

	private static DESEncrypt mEncrypt = new DESEncrypt();

	// in-app billing
	private static String OD_SKU = "coins.onedollar";
	private static String TD_SKU = "coins.twodollars";
	private static String FD_SKU = "coins.fivedollars";
	private static String ED_SKU = "coins.tendollars";
	private static String WD_SKU = "coins.twentydollars";

	public static void showGetCoins(final Activity activity,
			final IabHelper helper) {
		try {
			mCallingActivity = activity;
			mHelper = helper;
			Display display = activity.getWindowManager().getDefaultDisplay();
			final int swidth = display.getWidth();
			final int sheight = display.getHeight();

			// initialize in-app billing
			if (helper != null && !helper.isSetup()) {
				helper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
					public void onIabSetupFinished(IabResult result) {
						if (!result.isSuccess()) {
							Log.d(TAG, "In-app Billing setup failed: " + result);
							Toast.makeText(activity,
									"Could not initialize purchase, sorry.",
									Toast.LENGTH_LONG).show();
							mIabSetup = false;
							return;
						} else {
							Log.d(TAG, "In-app Billing is set up OK");
							mIabSetup = true;
						}
					}
				});
			}

			final Dialog dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.getcoins);
			dialog.getWindow().setBackgroundDrawable(null);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			RelativeLayout rl = (RelativeLayout) dialog
					.findViewById(R.id.coinsLayout);
			RelativeLayout rl2 = (RelativeLayout) dialog
					.findViewById(R.id.coinsContent);
			RelativeLayout.LayoutParams lp;
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.9),
					(int) (sheight * 0.9));
			lp.leftMargin = 0;
			lp.topMargin = 0;
			rl.updateViewLayout(rl2, lp);

			ImageView odview = (ImageView) dialog.findViewById(R.id.onedollar);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.45);
			lp.topMargin = (int) (sheight * 0.75);
			rl2.updateViewLayout(odview, lp);

			ImageView odimg = (ImageView) dialog
					.findViewById(R.id.threefivezerocoins);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.05);
			lp.topMargin = (int) (sheight * 0.75);
			rl2.updateViewLayout(odimg, lp);

			odview.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mIabSetup) {
						helper.launchPurchaseFlow(activity, OD_SKU, 10001,
								mPurchaseFinishedListener, "");
					} else {
						Toast.makeText(activity,
								"Could not initialize purchase, sorry.",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			ImageView tdview = (ImageView) dialog.findViewById(R.id.twodollars);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.45);
			lp.topMargin = (int) (sheight * 0.6);
			rl2.updateViewLayout(tdview, lp);

			ImageView tdimg = (ImageView) dialog
					.findViewById(R.id.sevenfivezerocons);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.05);
			lp.topMargin = (int) (sheight * 0.6);
			rl2.updateViewLayout(tdimg, lp);

			tdview.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mIabSetup) {
						helper.launchPurchaseFlow(activity, TD_SKU, 10002,
								mPurchaseFinishedListener, "");
					} else {
						Toast.makeText(activity,
								"Could not initialize purchase, sorry.",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			ImageView fdview = (ImageView) dialog
					.findViewById(R.id.fivedollars);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.45);
			lp.topMargin = (int) (sheight * 0.45);
			rl2.updateViewLayout(fdview, lp);

			ImageView fdimg = (ImageView) dialog
					.findViewById(R.id.twotripleocoins);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.05);
			lp.topMargin = (int) (sheight * 0.45);
			rl2.updateViewLayout(fdimg, lp);

			fdview.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mIabSetup) {
						helper.launchPurchaseFlow(activity, FD_SKU, 10003,
								mPurchaseFinishedListener, "");
					} else {
						Toast.makeText(activity,
								"Could not initialize purchase, sorry.",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			ImageView edview = (ImageView) dialog.findViewById(R.id.tendollars);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.45);
			lp.topMargin = (int) (sheight * 0.3);
			rl2.updateViewLayout(edview, lp);

			ImageView edimg = (ImageView) dialog
					.findViewById(R.id.fourfiveoocoin);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.05);
			lp.topMargin = (int) (sheight * 0.3);
			rl2.updateViewLayout(edimg, lp);

			edview.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mIabSetup) {
						helper.launchPurchaseFlow(activity, ED_SKU, 10004,
								mPurchaseFinishedListener, "");
					} else {
						Toast.makeText(activity,
								"Could not initialize purchase, sorry.",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			ImageView wdview = (ImageView) dialog
					.findViewById(R.id.twentydollars);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.45);
			lp.topMargin = (int) (sheight * 0.15);
			rl2.updateViewLayout(wdview, lp);

			ImageView wdimg = (ImageView) dialog.findViewById(R.id.oneooo);
			lp = new RelativeLayout.LayoutParams((int) (swidth * 0.4),
					(int) (sheight * 0.12));
			lp.leftMargin = (int) (swidth * 0.05);
			lp.topMargin = (int) (sheight * 0.15);
			rl2.updateViewLayout(wdimg, lp);

			wdview.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mIabSetup) {
						helper.launchPurchaseFlow(activity, WD_SKU, 10005,
								mPurchaseFinishedListener, "");
					} else {
						Toast.makeText(activity,
								"Could not initialize purchase, sorry.",
								Toast.LENGTH_LONG).show();
					}
				}
			});

			ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
			lp = new RelativeLayout.LayoutParams(70, 70);
			lp.leftMargin = (int) (swidth * 0.9) - 75;
			lp.topMargin = 5;
			rl.updateViewLayout(cancel, lp);

			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			dialog.show();

		} catch (Exception e) {
			Log.e(TAG, "could not show get coins menu", e);
		}
	}

	public static void showHelpers(final Activity activity,
			final IabHelper helper) {
		try {

			mCallingActivity = activity;
			mHelper = helper;
			Display display = activity.getWindowManager().getDefaultDisplay();
			final int swidth = display.getWidth();
			final int sheight = display.getHeight();

			final Dialog dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.helpers);
			dialog.getWindow().setBackgroundDrawable(null);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			RelativeLayout rl = (RelativeLayout) dialog
					.findViewById(R.id.helpersLayout);
			RelativeLayout rl2 = (RelativeLayout) dialog
					.findViewById(R.id.helpersContent);
			RelativeLayout.LayoutParams lp;
			lp = new RelativeLayout.LayoutParams(400, 500);
			lp.leftMargin = 0;
			lp.topMargin = 0;
			rl.updateViewLayout(rl2, lp);

			ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
			lp = new RelativeLayout.LayoutParams(70, 70);
			lp.leftMargin = 325;
			lp.topMargin = 0;
			rl.updateViewLayout(cancel, lp);

			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			ImageView uncoverpic = (ImageView) dialog
					.findViewById(R.id.uncoverpic);
			lp = new RelativeLayout.LayoutParams(150, 75);
			lp.leftMargin = 50;
			lp.topMargin = 100;
			rl2.updateViewLayout(uncoverpic, lp);

			uncoverpic.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PlayGame pg = (PlayGame) activity;
					pg.showImage();
					dialog.dismiss();
				}
			});

			ImageView fiftycoin = (ImageView) dialog
					.findViewById(R.id.fiftycoin);
			lp = new RelativeLayout.LayoutParams(150, 75);
			lp.leftMargin = 225;
			lp.topMargin = 100;
			rl2.updateViewLayout(fiftycoin, lp);

			ImageView revealletter = (ImageView) dialog
					.findViewById(R.id.revealletter);
			lp = new RelativeLayout.LayoutParams(150, 75);
			lp.leftMargin = 50;
			lp.topMargin = 190;
			rl2.updateViewLayout(revealletter, lp);

			revealletter.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PlayGame pg = (PlayGame) activity;
					pg.revealLetter();
					dialog.dismiss();
				}
			});

			ImageView fiftycoin1 = (ImageView) dialog
					.findViewById(R.id.fiftycoin1);
			lp = new RelativeLayout.LayoutParams(150, 75);
			lp.leftMargin = 225;
			lp.topMargin = 190;
			rl2.updateViewLayout(fiftycoin1, lp);

			ImageView removeletter = (ImageView) dialog
					.findViewById(R.id.removeletter);
			lp = new RelativeLayout.LayoutParams(150, 60);
			lp.leftMargin = 50;
			lp.topMargin = 280;
			rl2.updateViewLayout(removeletter, lp);

			removeletter.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PlayGame pg = (PlayGame) activity;
					pg.hideLetters();
					dialog.dismiss();
				}
			});

			ImageView seventyfive = (ImageView) dialog
					.findViewById(R.id.seventyfive);
			lp = new RelativeLayout.LayoutParams(150, 75);
			lp.leftMargin = 225;
			lp.topMargin = 280;
			rl2.updateViewLayout(seventyfive, lp);

			ImageView getmorecoins = (ImageView) dialog
					.findViewById(R.id.getmorecoins);
			lp = new RelativeLayout.LayoutParams(200, 75);
			lp.leftMargin = 100;
			lp.topMargin = 380;
			rl2.updateViewLayout(getmorecoins, lp);

			getmorecoins.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showGetCoins(activity, helper);
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception e) {
			Log.e(TAG, "could not show get coins menu", e);
		}
	}

	public static void showNotEnoughCoins(final Activity activity,
			final IabHelper helper, String message) {
		try {

			mCallingActivity = activity;
			mHelper = helper;
			Display display = activity.getWindowManager().getDefaultDisplay();
			final int swidth = display.getWidth();
			final int sheight = display.getHeight();

			final Dialog dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.notenoughcoins);
			dialog.getWindow().setBackgroundDrawable(null);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			RelativeLayout rl = (RelativeLayout) dialog
					.findViewById(R.id.notEnoughLayout);
			RelativeLayout rl2 = (RelativeLayout) dialog
					.findViewById(R.id.notEnoughContent);
			RelativeLayout.LayoutParams lp;
			lp = new RelativeLayout.LayoutParams(400, 300);
			lp.leftMargin = 0;
			lp.topMargin = 0;
			rl.updateViewLayout(rl2, lp);

			ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
			lp = new RelativeLayout.LayoutParams(50, 50);
			lp.leftMargin = 345;
			lp.topMargin = 0;
			rl.updateViewLayout(cancel, lp);

			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			TextView notenoughtext = (TextView) dialog
					.findViewById(R.id.notenoughtext);
			notenoughtext.setText(message);

			ImageView getmorecoins = (ImageView) dialog
					.findViewById(R.id.getmorecoins);
			getmorecoins.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showGetCoins(activity, helper);
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception e) {
			Log.e(TAG, "could not show get coins menu", e);
		}
	}

	public static void showConfirmLevel(final LevelActivity activity,
			String message, final long level) {
		try {

			mCallingActivity = activity;
			Display display = activity.getWindowManager().getDefaultDisplay();
			final int swidth = display.getWidth();
			final int sheight = display.getHeight();

			final Dialog dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.confirmlevel);
			dialog.getWindow().setBackgroundDrawable(null);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			RelativeLayout rl = (RelativeLayout) dialog
					.findViewById(R.id.confirmLayout);
			RelativeLayout rl2 = (RelativeLayout) dialog
					.findViewById(R.id.confirmContent);
			RelativeLayout.LayoutParams lp;
			lp = new RelativeLayout.LayoutParams(400, 300);
			lp.leftMargin = 0;
			lp.topMargin = 0;
			rl.updateViewLayout(rl2, lp);

			ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
			lp = new RelativeLayout.LayoutParams(50, 50);
			lp.leftMargin = 345;
			lp.topMargin = 0;
			rl.updateViewLayout(cancel, lp);

			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			TextView confirmtext = (TextView) dialog
					.findViewById(R.id.confirmtext);
			confirmtext.setText(message);

			ImageView confirm = (ImageView) dialog
					.findViewById(R.id.confirmimage);
			confirm.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					activity.doBuyLevel(level);
					dialog.dismiss();
				}
			});

			dialog.show();

		} catch (Exception e) {
			Log.e(TAG, "could not show get coins menu", e);
		}
	}

	static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase attempt failed : " + result.getMessage());
				return;
			} else if (purchase.getSku().equals(OD_SKU)) {
				consumeItemOD();
			} else if (purchase.getSku().equals(TD_SKU)) {
				consumeItemTD();
			} else if (purchase.getSku().equals(FD_SKU)) {
				consumeItemFD();
			} else if (purchase.getSku().equals(ED_SKU)) {
				consumeItemED();
			} else if (purchase.getSku().equals(WD_SKU)) {
				consumeItemWD();
			}
		}
	};

	public static void consumeItemOD() {
		mHelper.queryInventoryAsync(mReceivedInventoryListenerOD);
	}

	static IabHelper.QueryInventoryFinishedListener mReceivedInventoryListenerOD = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase inventory failed : " + result.getMessage());
			} else {
				mHelper.consumeAsync(inventory.getPurchase(OD_SKU),
						mConsumeFinishedListenerOD);
			}
		}
	};

	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerOD = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				// add 350 coins to the score
				Log.d(TAG, "successfully bought 350 coins.");
				addCoins(350);
			} else {
				Log.e(TAG, "purchase consuming failed : " + result.getMessage());
			}
		}
	};

	public static void consumeItemTD() {
		mHelper.queryInventoryAsync(mReceivedInventoryListenerTD);
	}

	static IabHelper.QueryInventoryFinishedListener mReceivedInventoryListenerTD = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase inventory failed : " + result.getMessage());
			} else {
				mHelper.consumeAsync(inventory.getPurchase(TD_SKU),
						mConsumeFinishedListenerTD);
			}
		}
	};

	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerTD = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				// add 750 coins to the score
				Log.d(TAG, "successfully bought 750 coins.");
				addCoins(750);
			} else {
				Log.e(TAG, "purchase consuming failed : " + result.getMessage());
			}
		}
	};

	public static void consumeItemFD() {
		mHelper.queryInventoryAsync(mReceivedInventoryListenerFD);
	}

	static IabHelper.QueryInventoryFinishedListener mReceivedInventoryListenerFD = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase inventory failed : " + result.getMessage());
			} else {
				mHelper.consumeAsync(inventory.getPurchase(FD_SKU),
						mConsumeFinishedListenerFD);
			}
		}
	};

	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerFD = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				// add 2000 coins to the score
				Log.d(TAG, "successfully bought 2000 coins.");
				addCoins(2000);
			} else {
				Log.e(TAG, "purchase consuming failed : " + result.getMessage());
			}
		}
	};

	public static void consumeItemED() {
		mHelper.queryInventoryAsync(mReceivedInventoryListenerED);
	}

	static IabHelper.QueryInventoryFinishedListener mReceivedInventoryListenerED = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase inventory failed : " + result.getMessage());
			} else {
				mHelper.consumeAsync(inventory.getPurchase(ED_SKU),
						mConsumeFinishedListenerED);
			}
		}
	};

	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerED = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				// add 4500 coins to the score
				Log.d(TAG, "successfully bought 4500 coins.");
				addCoins(4500);
			} else {
				Log.e(TAG, "purchase consuming failed : " + result.getMessage());
			}
		}
	};

	public static void consumeItemWD() {
		mHelper.queryInventoryAsync(mReceivedInventoryListenerWD);
	}

	static IabHelper.QueryInventoryFinishedListener mReceivedInventoryListenerWD = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			if (result.isFailure()) {
				Log.e(TAG, "purchase inventory failed : " + result.getMessage());
			} else {
				mHelper.consumeAsync(inventory.getPurchase(WD_SKU),
						mConsumeFinishedListenerWD);
			}
		}
	};

	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerWD = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				// add 10000 coins to the score
				Log.d(TAG, "successfully bought 10000 coins.");
				addCoins(10000);
			} else {
				Log.e(TAG, "purchase consuming failed : " + result.getMessage());
			}
		}
	};

	public static void addCoins(int quantity) {
		SharedPreferences mPrefs = mCallingActivity.getSharedPreferences(
				LevelActivity.PREFS_NAME, 0);
		int coins = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
				"CoinValue", mEncrypt.encrypt("0"))));
		coins += quantity;
		mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + coins))
				.commit();
	}

}
