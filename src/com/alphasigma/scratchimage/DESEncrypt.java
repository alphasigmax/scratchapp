/*******************************************************************************
 * Copyright 2013 Chevil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.alphasigma.scratchimage;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

/**
 * Utility class to crypt and decrypt scores and more
 * 
 * @author chevil
 */
public class DESEncrypt {

	private static final String TAG = "Scratch Celebrities";

	private Context mContext;
	private DESKeySpec mKeySpec = null;
	private SecretKeyFactory mKeyFactory = null;
	private SecretKey mKey = null;

	public DESEncrypt() {
		try {
			mKeySpec = new DESKeySpec(TAG.getBytes("UTF8"));
			mKeyFactory = SecretKeyFactory.getInstance("DES");
			mKey = mKeyFactory.generateSecret(mKeySpec);
		} catch (Exception e) {
			Log.e(TAG, "could not create cipher classes", e);
		}
	}

	public String encrypt(String input) {
		try {
			byte[] cleartext = input.getBytes("UTF8");
			Cipher cipher = Cipher.getInstance("DES"); // cipher is not thread
														// safe
			cipher.init(Cipher.ENCRYPT_MODE, mKey);
			return Base64.encodeToString(cipher.doFinal(cleartext),
					Base64.DEFAULT);
		} catch (Exception e) {
			Log.e(TAG, "could not encrypt data", e);
			return "";
		}
	}

	public String decrypt(String input) {
		try {
			byte[] encryptedPwdBytes = Base64.decode(input, Base64.DEFAULT);
			Cipher cipher = Cipher.getInstance("DES");// cipher is not thread
														// safe
			cipher.init(Cipher.DECRYPT_MODE, mKey);
			return new String(cipher.doFinal(encryptedPwdBytes));
		} catch (Exception e) {
			Log.e(TAG, "could not decrypt data", e);
			return "";
		}
	}

}
