/*******************************************************************************
 * Copyright 2013 Winson Tan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.alphasigma.scratchimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This view start with full gray color bitmap and onTouch to make it
 * transparent
 * 
 * @author winsontan520
 */
public class WScratchView extends SurfaceView implements SurfaceHolder.Callback {

	private static final String TAG = "Scratch Celebrities";

	// default value constants
	private final int DEFAULT_COLOR = 0xff444444; // default color is dark gray
	private final int DEFAULT_REVEAL_SIZE = 30;

	private Context mContext;
	private PlayGame mActivity = null;
	private int mOverlayColor;
	private int mRevealSize;
	private boolean mIsScratchable = true;
	private int startX = 0;
	private int startY = 0;
	private boolean mScratchStart = false;
	private Bitmap mBitmap = null;
	private Bitmap mMaskBitmap;
	private byte[] mMask;
	private int mWidth;
	private int mHeight;
	private int mTotalPixels;
	private int mScratchedPixels;

	public WScratchView(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
		init(ctx, attrs);
	}

	public WScratchView(Context context) {
		super(context);
		init(context, null);
	}

	private void init(Context context, AttributeSet attrs) {
		mContext = context;

		// default value
		mOverlayColor = DEFAULT_COLOR;
		mRevealSize = DEFAULT_REVEAL_SIZE;

		setZOrderOnTop(true);
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		holder.setFormat(PixelFormat.TRANSPARENT);

	}

	public void setBitmap(Bitmap bitmap, boolean showImage) {
		Canvas c = null;
		mBitmap = bitmap;
		mMaskBitmap = Bitmap.createBitmap(mBitmap);
		mWidth = mBitmap.getWidth();
		mHeight = mBitmap.getHeight();
		mTotalPixels = mWidth * mHeight;
		mMask = new byte[mWidth * mHeight];
		if (!showImage) {
			Log.v(TAG, "setting to grey");
			mMaskBitmap.eraseColor(mOverlayColor);
			mScratchedPixels = 0;
		} else {
			mScratchedPixels = mTotalPixels;
		}
		try {
			c = getHolder().lockCanvas(null);
			synchronized (getHolder()) {
				if (c != null) {
					onDraw(c);
				}
			}
		} finally {
			if (c != null) {
				getHolder().unlockCanvasAndPost(c);
			}
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawBitmap(mMaskBitmap, new Rect(0, 0, mWidth, mHeight),
				new RectF(0, 0, canvas.getWidth(), canvas.getHeight()),
				new Paint());
	}

	@Override
	public boolean onTouchEvent(MotionEvent me) {

		Canvas c = null;

		synchronized (getHolder()) {
			if (!mIsScratchable) {
				// update progress ( just to make a klang )
				int progress = 100 - (mScratchedPixels * 100) / mTotalPixels;
				if (mActivity != null) {
					mActivity.setScratchProgress(progress);
				}
				return true;
			}

			final int pointerCount = (int) me.getPointerCount();
			int p;

			switch (me.getAction()) {
			case MotionEvent.ACTION_DOWN:
				startX = (int) me.getX();
				startY = (int) me.getY();
				break;

			case MotionEvent.ACTION_MOVE:
				for (p = 0; p < pointerCount; p++) {
					int minX = Math.min(startX, (int) me.getX(p));
					int maxX = Math.max(startX, (int) me.getX(p));
					int minY = Math.min(startY, (int) me.getY(p));
					int maxY = Math.max(startY, (int) me.getY(p));
					double slope;
					if ((maxX - minX) > 0) {
						slope = (double) (maxY - minY) / (double) (maxX - minX);
					} else {
						slope = 0;
					}
					for (int ix = minX; ix < maxX; ix++) {
						// calculate the point on the line
						int iy = minY + (int) ((double) (ix - minX) * slope);
						if (iy * mWidth + ix >= mHeight * mWidth
								|| iy * mWidth + ix < 0) // some imprecision
															// might occur
						{
							Log.v(TAG, "wrong point : " + ix + "," + iy);
							continue;
						}
						mask(ix, iy);
					}
					// Log.v( TAG, "move from : " + minX + "," + minY + " to " +
					// maxX + "," + maxY + " sp : " + mScratchedPixels );
					startX = (int) me.getX(p);
					startY = (int) me.getY(p);
				}

				// update progress
				int progress = 100 - (mScratchedPixels * 100) / mTotalPixels;
				if (mActivity != null) {
					mActivity.setScratchProgress(progress);
				}
				break;

			case MotionEvent.ACTION_UP:
				startX = -1;
				startY = -1;
				break;
			}
		}
		try {
			c = getHolder().lockCanvas(null);
			synchronized (getHolder()) {
				if (c != null) {
					onDraw(c);
				}
			}
		} finally {
			if (c != null) {
				getHolder().unlockCanvasAndPost(c);
			}
		}
		return true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// do nothing
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		Canvas c = null;

		try {
			c = getHolder().lockCanvas(null);
			synchronized (getHolder()) {
				if (c != null) {
					onDraw(c);
				}
			}
		} finally {
			if (c != null) {
				getHolder().unlockCanvasAndPost(c);
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		Log.v(TAG, "surface destroyed");
	}

	public boolean isScratchable() {
		return mIsScratchable;
	}

	public void setScratchable(boolean flag) {
		mIsScratchable = flag;
	}

	public void setOverlayColor(int ResId) {
		mOverlayColor = ResId;
	}

	public void setRevealSize(int size) {
		mRevealSize = size;
	}

	public void setActivity(PlayGame activity) {
		mActivity = activity;
	}

	public void mask(int ix, int iy) {
		for (int iix = Math.max(0, ix - mRevealSize); iix < Math.min(mWidth, ix
				+ mRevealSize); iix++) {
			for (int iiy = Math.max(0, iy - mRevealSize); iiy < Math.min(
					mHeight, iy + mRevealSize); iiy++) {
				if (mMask[iiy * mWidth + iix] == 0) {
					int d = (int) Math.sqrt(Math.pow((iix - ix), 2)
							+ Math.pow((iiy - iy), 2));
					if (d <= mRevealSize) {
						mMask[iiy * mWidth + iix] = 1;
						mScratchedPixels++;
						mMaskBitmap.setPixel(iix, iiy,
								mBitmap.getPixel(iix, iiy));
					}
				}
			}
		}
	}

}
