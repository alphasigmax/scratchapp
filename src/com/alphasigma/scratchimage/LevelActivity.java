package com.alphasigma.scratchimage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alphasigma.scratchimage.inappbilling.util.IabHelper;

public class LevelActivity extends Activity implements OnItemClickListener {

	private final String TAG = "Scratch Celebrities";
	private ListView listView;
	private long position;
	private TextView cointext;
	private long mCoins;
	private SharedPreferences mPrefs;
	private int mNbLevels = 0;
	private int mLevelPrice = 150;
	private String mEnigmas = "enigmas";
	public static final String PREFS_NAME = "MyPrefsFile";
	private DESEncrypt mEncrypt = new DESEncrypt();
	private CustomListAdapter customListAdapter;
	ImageView cancel, getmorecoins, uncoverpic, fiftycoin, revalletter,
			fiftycoin1, removeletter, seventyfive, cancel1, oneooo,
			ooonetriplenine, fourfiveoocoin, ootriplenine, twotripleocoins,
			ofourninenine, sevenfivezerocons, oooneninenine,
			threefivezerocoins, ooninenine;

	// in-app billing
	private static String appKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlsat8RmPDfO7ygcRtY0wz6SNjg9WXmoZ6J1ClDD0drKzDfGTbxWpxaGdwHpy42vbih0Pbnt9SFld08ZzTO0ESWvKcs3QfC7Sgc+MeAAUioH6HmGhqwZfb5DXXDn1IgI98UsPwWBnnm1Fu9aY4ZPGRrH7Iog0GgYo31lqqQKhyIUAFr0KtsxBxaEkWnLfWe21Jfk6eSKG5g4qLQ5EcUztnDuIAAMcq5ka5qyGgqMAvVgys19Q5LrK0KtC0xnVcWwo5WtnpxtWS9tfA/GMc03UQcYMfQT+ihjzEpCJogKZYIHDS4oBqYaQifDmC30DWnU280UxvckvCvug0QAWt3N/vwIDAQAB";
	public static IabHelper mHelper = null;
	private Handler mHandler = null;

	private Runnable mUpdateCoins = new Runnable() {
		@Override
		public void run() {
			if (cointext != null) {
				/**
				 * 500 is the default amount now, It was 0 , Change Date : 5th
				 * Feb,2014
				 * 
				 */
				String gettingcoin = mEncrypt.decrypt(mPrefs.getString(
						"CoinValue", mEncrypt.encrypt("500")));
				cointext.setText(gettingcoin);
				mCoins = Integer.parseInt(gettingcoin);
			}
			if (mHandler != null) {
				mHandler.postDelayed(this, 1000);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mPrefs = getSharedPreferences(LevelActivity.PREFS_NAME, 0);

		if (mHelper == null) {
			mHelper = new IabHelper(this, appKey);
		}

		mHandler = new Handler();
		mHandler.postDelayed(mUpdateCoins, 1000);

		try {
			String firstTime = mEncrypt.decrypt(mPrefs.getString("FirstTime",
					mEncrypt.encrypt("y")));
			Random r = new Random();
			if (firstTime.equals("y")) {
				Log.v(TAG, "loading levels...");
				SharedPreferences.Editor editor = mPrefs.edit();
				for (int il = 0; il < 1000; il++) {
					String jsontext = loadJSONFromAsset(mEnigmas + il + ".json");
					if (jsontext == null) {
						break;
					}
					mNbLevels++;
					JSONArray json = new JSONArray(jsontext);
					editor.putString("NbEnigmas" + il,
							mEncrypt.encrypt("" + json.length()));
					editor.putString("NbRevealed" + il, mEncrypt.encrypt("0"));
					editor.putString("HideLetters" + il, mEncrypt.encrypt("n"));
					editor.putString("ShowImage" + il, mEncrypt.encrypt("n"));
					editor.putString("Level" + il, mEncrypt.encrypt("0"));
					if (il == 0) {
						editor.putString("Lock" + il, mEncrypt.encrypt("" + il));
					} else {
						editor.putString("Lock" + il,
								mEncrypt.encrypt("-" + il));
					}
				}
				editor.putString("FirstTime", mEncrypt.encrypt("n"));
				editor.putString("NbLevels", mEncrypt.encrypt("" + mNbLevels));
				editor.commit();
			} else {
				mNbLevels = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
						"NbLevels", mEncrypt.encrypt("0"))));
			}
			Log.v(TAG, "got " + mNbLevels + " levels");
		} catch (Exception e) {
			Log.e(TAG, "Couldn't write initial values", e);
			this.finish();
		}

		setContentView(R.layout.activity_level);
		setViews();
	}

	private void setViews() {
		Typeface type = Typeface.createFromAsset(getAssets(), "font.ttf");
		cointext = (TextView) findViewById(R.id.cointext);
		cointext.setTypeface(type);
		cointext.setText(mEncrypt.decrypt(mPrefs.getString("CoinValue",
				mEncrypt.encrypt("0"))));
		mCoins = Long.parseLong(mEncrypt.decrypt(mPrefs.getString("CoinValue",
				mEncrypt.encrypt("0"))));

		listView = (ListView) findViewById(R.id.listView);
		List<String> sizeList = new ArrayList<String>(mNbLevels);
		for (int il = 0; il < mNbLevels; il++) {
			sizeList.add(new String("Level " + (il + 1)));
		}
		customListAdapter = new CustomListAdapter(this, sizeList);
		customListAdapter.setSharedPreferences(mPrefs);
		listView.setAdapter(customListAdapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		position = pos;
		int isLocked = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
				"Lock" + position, mEncrypt.encrypt("-1"))));
		if (isLocked == pos) {
			int level = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
					"Level" + position, mEncrypt.encrypt("0"))));
			int nbEnigmas = Integer.parseInt(mEncrypt.decrypt(mPrefs.getString(
					"NbEnigmas" + position, mEncrypt.encrypt("0"))));
			OurSoundPlayer.playSound(this, R.raw.clang);
			if (level + 1 < nbEnigmas) {
				try {
					Intent i = new Intent(LevelActivity.this, PlayGame.class);
					i.putExtra("album", position);
					startActivity(i);
					this.finish();
				} catch (Exception e) {
					Log.e(TAG, "Could not start activity", e);
				}
			}
		} else {
			if (mCoins < mLevelPrice) {
				OurSoundPlayer.playSound(this, R.raw.clang);
				ShowMenus.showGetCoins(this, mHelper);
			} else {
				ShowMenus.showConfirmLevel(this,
						"Are you sure you want to buy that level for : "
								+ mLevelPrice + " coins?", position);
			}
		}
	}

	public void doBuyLevel(long level) {
		mCoins -= mLevelPrice;
		mPrefs.edit().putString("CoinValue", mEncrypt.encrypt("" + mCoins))
				.commit();
		mPrefs.edit()
				.putString("Lock" + level, mEncrypt.encrypt("" + position))
				.commit();
		try {
			Toast.makeText(LevelActivity.this,
					"You unlocked that level for : " + mLevelPrice + " coins.",
					Toast.LENGTH_LONG).show();
			Intent i = new Intent(LevelActivity.this, LevelActivity.class);
			startActivity(i);
			this.finish();
		} catch (Exception e) {
			Log.e(TAG, "Could not start activity", e);
		}
	}

	public void back(View v) {
		startActivity(new Intent(LevelActivity.this, PlayActivity.class));
		finish();
	}

	public String loadJSONFromAsset(String filename) {
		String json = null;
		try {
			InputStream is = getAssets().open(filename);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			json = new String(buffer, "UTF-8");
		} catch (IOException ex) {
			Log.e(TAG, "couldn't read json from assets : " + filename);
			return null;
		}
		return json;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper == null
				|| !mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
		String gettingcoin = mEncrypt.decrypt(mPrefs.getString("CoinValue",
				mEncrypt.encrypt("0")));
		cointext.setText(gettingcoin);
		cointext.invalidate();
		mCoins = Integer.parseInt(gettingcoin);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
		if (mHandler != null) {
			mHandler.removeCallbacks(mUpdateCoins);
			mHandler = null;
		}
	}

	public void showGetCoins(View v) {
		ShowMenus.showGetCoins(this, mHelper);
	}

}
