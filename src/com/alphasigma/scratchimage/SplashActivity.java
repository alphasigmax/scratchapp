package com.alphasigma.scratchimage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class SplashActivity extends Activity {

	private Thread mSplashThread;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_splash);
		final SplashActivity sPlashScreen = this;
		OurSoundPlayer.playSound(this, R.raw.clang);
		mythread();

	}

	public void mythread() {
		// The thread to wait for splash screen events
		mSplashThread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						// Wait given period of time or exit on touc
						Thread.sleep(3000);
					}
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				} finally {
					finish();

					// Run next activity
					Intent intent = new Intent(SplashActivity.this,
							PlayActivity.class);
					startActivity(intent);
					// stop();
				}
			}
		};

		mSplashThread.start();
	}

	/**
	 * Processes splash screen touch events
	 */
	@Override
	public boolean onTouchEvent(MotionEvent evt) {
		if (evt.getAction() == MotionEvent.ACTION_DOWN) {
			synchronized (mSplashThread) {
				// mSplashThread.notifyAll();
			}
		}
		return true;
	}

	public void gotosplash() {

	}
}
