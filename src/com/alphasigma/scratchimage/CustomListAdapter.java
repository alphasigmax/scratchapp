package com.alphasigma.scratchimage;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alphasigma.scratchimage.R.drawable;

public class CustomListAdapter extends BaseAdapter {

	private Context context;
	private List<String> imageUrlList;
	int position;
	public static final String TAG = "Scratch Celebrities";
	public static final String PREFS_NAME = "MyPrefsFile";
	private DESEncrypt mEncrypt = new DESEncrypt();
	private SharedPreferences settings;

	public CustomListAdapter(Context context, List<String> data) {
		super();
		this.context = context;
		this.imageUrlList = data;
	}

	public void setSharedPreferences(SharedPreferences prefs) {
		settings = prefs;
	}

	public void setData(List<String> data) {
		this.imageUrlList = data;
	}

	@Override
	public int getCount() {
		return imageUrlList == null ? 3 : imageUrlList.size();
	}

	@Override
	public Object getItem(int pos) {
		return new String("");
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		ImageView img;
		TextView txt;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.item_listview, parent, false);
		img = (ImageView) convertView.findViewById(R.id.imageView);
		txt = (TextView) convertView.findViewById(R.id.textView);
		position = pos;

		int isLocked = Integer.parseInt(mEncrypt.decrypt(settings.getString(
				"Lock" + position, mEncrypt.encrypt("-1"))));

		int level = Integer.parseInt(mEncrypt.decrypt(settings.getString(
				"Level" + position, mEncrypt.encrypt("0"))));
		int nbEnigmas = Integer.parseInt(mEncrypt.decrypt(settings.getString(
				"NbEnigmas" + position, mEncrypt.encrypt("0"))));
		if (level == nbEnigmas) {
			img.setBackgroundResource(drawable.level_done);
			txt.setText("" + level + "/" + nbEnigmas);
		} else if (isLocked == pos) {
			img.setBackgroundResource(drawable.open_locked);
			txt.setText("" + (level + 1) + "/" + nbEnigmas);
		} else {
			img.setBackgroundResource(drawable.folder_locked);
			txt.setText("");
		}

		return convertView;
	}
}
